#!/bin/sh

# Encrypt a file with aesgcm

usage(){
	echo "Usage: $0 -e|-d -k <key> -i <infile> -o <outfile>"
	exit 2
}

aesgcm_enc(){
	cat "$1" | ./aesgcm "$3" enc > "$2"
}

aesgcm_dec(){
	cat "$1" | ./aesgcm "$3" > "$2"
}

unset ACTION KEY INFILE OUTFILE

while getopts 'edk:i:o:' c
do
	case $c in
		e) ACTION="enc" ;;
		d) ACTION="dec" ;;
		k) KEY=$OPTARG ;;
		i) INFILE=$OPTARG ;;
		o) OUTFILE=$OPTARG ;;
	esac
done

[ -z "$ACTION" ] && usage
[ "$ACTION" = "dec" ] && [ -z "$KEY" ] && usage
[ -z "$KEY" ] && KEY="$(hexdump -n 44 -e '11/4 "%08x"' /dev/urandom)"
[ -z "$INFILE" ] && INFILE="-"
[ -z "$OUTFILE" ] && OUTFILE="-"

# AESGCMKEY="$(hexdump -n 44 -e '11/4 "%08x"' /dev/urandom)"

case $ACTION in
	enc)	aesgcm_enc "$INFILE" "$OUTFILE" "$KEY" ;;
	dec)	aesgcm_dec "$INFILE" "$OUTFILE" "$KEY" ;;
esac

echo "Key: $KEY" >&2
echo "Done." >&2
